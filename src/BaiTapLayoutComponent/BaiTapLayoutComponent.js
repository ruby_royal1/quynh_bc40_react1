import React, { Component } from 'react'
import Content from './Content'
import Footer from './Footer'
import Header from './Header'
import Navbar from './Navbar'

export default class BaiTapLayoutComponent extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <Header />
                <Content />
                <Footer />

            </div>
        )
    }
}
